#!/bin/bash
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Build script for building Breakpad
# Generally run in Taskcluster, but split out to a separate script
# so it can be run for local builds if necessary without assuming
# the Taskcluster environment.

# any failures in this script should cause the build to fail
set -v -e -x

export MAKEFLAGS
MAKEFLAGS=-j$(getconf _NPROCESSORS_ONLN)

# Checkout and build Breakpad
echo "PREFIX: ${PREFIX:=$(pwd)/build/breakpad}"
cd breakpad
./autogen.sh
./configure --prefix="${PREFIX}"
make install
cd ..
cp breakpad/src/third_party/libdisasm/libdisasm.a "${PREFIX}"lib/
